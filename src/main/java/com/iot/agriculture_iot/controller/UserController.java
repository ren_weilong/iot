package com.iot.agriculture_iot.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.iot.agriculture_iot.bean.User;
import com.iot.agriculture_iot.mapper.UserMapper;
import com.iot.agriculture_iot.util.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController("user")
public class UserController {
    @Autowired
    UserMapper userMapper;

    @PostMapping("/add")
    public ApiResponse addUser(@RequestBody User user) {
        userMapper.insert(user);
        return ApiResponse.success();
    }

    @PostMapping("/modify")
    public ApiResponse modifyUser(@RequestBody User user) {
        userMapper.updateById(user);
        return ApiResponse.success();
    }

    @GetMapping("/delete")
    public ApiResponse deleteUser(@RequestParam(value = "id") Integer id) {
        userMapper.deleteById(id);
        return ApiResponse.success();
    }

    @GetMapping("/all")
    public ApiResponse allUsers(@RequestParam Integer page, @RequestParam Integer size) {
        Page<User> userPage = new Page<>(page, size);
        return ApiResponse.success(userMapper.selectPage(userPage, null).getRecords());
    }
}
