package com.iot.agriculture_iot.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.segments.MergeSegments;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.iot.agriculture_iot.bean.IOT;
import com.iot.agriculture_iot.bean.Sensor;
import com.iot.agriculture_iot.mapper.IotMapper;
import com.iot.agriculture_iot.mapper.SensorMapper;
import com.iot.agriculture_iot.util.ApiResponse;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("/iot")
public class IotController {
    @Autowired
    SensorMapper sensorMapper;
    @Autowired
    IotMapper iotMapper;

    @GetMapping("/meters")
    public ApiResponse getMeters(@RequestParam Integer page, @RequestParam Integer size) {
        Page<IOT> iotPage = new Page<>(page, size);
        List<IOT> records = iotMapper.selectPage(iotPage, null).getRecords();

        return ApiResponse.success(records);
    }

    @PostMapping("/set")
    public ApiResponse setSensor(@RequestBody JSONObject params) {
        // {"sensor":"temperature","threshold":10}
        String sensor = params.getString("sensor");
        Integer auto = params.getInteger("auto");
        sensorMapper.updateAutoBySensor(sensor, auto);
        return ApiResponse.success();
    }

    @GetMapping("/threshold")
    public ApiResponse getThreshold() {
        return ApiResponse.success(sensorMapper.getAll());
    }

    @PostMapping("/set/threshold")
    public ApiResponse setThreshold(@RequestBody JSONObject params) {
        String sensor = params.getString("sensor");
        Integer threshold = params.getInteger("threshold");
        sensorMapper.updateThresholdBySensor(sensor, threshold);
        return ApiResponse.success();
    }
}
