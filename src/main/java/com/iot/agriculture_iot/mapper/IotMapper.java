package com.iot.agriculture_iot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iot.agriculture_iot.bean.IOT;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IotMapper extends BaseMapper<IOT> {
}
