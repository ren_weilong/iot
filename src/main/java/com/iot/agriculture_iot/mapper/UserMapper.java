package com.iot.agriculture_iot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iot.agriculture_iot.bean.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {

}
