package com.iot.agriculture_iot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iot.agriculture_iot.bean.Sensor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SensorMapper extends BaseMapper<Sensor> {

    @Select(value = "select * from sensor")
    List<Sensor> getAll();

    @Update(value = "update sensor set threshold=#{threshold} where sensor=#{sensor}")
    void updateThresholdBySensor(@Param("sensor") String sensor, @Param("threshold") Integer threshold);

    @Update(value = "update sensor set auto=#{auto} where sensor=#{sensor}")
    void updateAutoBySensor(@Param("sensor") String sensor, @Param("auto") Integer auto);
}
