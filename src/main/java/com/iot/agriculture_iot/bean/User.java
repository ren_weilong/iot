package com.iot.agriculture_iot.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "user")
public class User {
    @TableId("id")//数据库主键名称
    private Integer id;
    private String username;
    private String password;
}
