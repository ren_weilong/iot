package com.iot.agriculture_iot.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

@Data
@TableName(value = "iot")
public class IOT {
    //5个U8数据 （温度，湿度，烟雾，光照，土壤湿度）
    @TableId("id")//数据库主键名称
    private Integer id;
    private Integer temperature;
    private Integer humidity;
    private Integer smoke;
    private Integer light;
    private Integer soilMoisture;

    static public IOT formatBy(List<String> list) {
        IOT iot = new IOT();
        iot.setTemperature(Integer.parseInt(list.get(5), 16));
        iot.setHumidity(Integer.parseInt(list.get(6), 16));
        iot.setSmoke(Integer.parseInt(list.get(7), 16));
        iot.setLight(Integer.parseInt(list.get(8), 16));
        iot.setSoilMoisture(Integer.parseInt(list.get(9), 16));
        return iot;
    }
}
