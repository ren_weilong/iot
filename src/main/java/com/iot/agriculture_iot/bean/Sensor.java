package com.iot.agriculture_iot.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "sensor")
public class Sensor {
    @TableId("id")//数据库主键名称
    private Integer id;
    private String sensor;
    private Integer threshold; //阈值
    private Integer auto; //是否自动报警，默认否
}
