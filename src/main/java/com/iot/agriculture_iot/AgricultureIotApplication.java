package com.iot.agriculture_iot;

import com.iot.agriculture_iot.mapper.IotMapper;
import com.iot.agriculture_iot.util.MqttSubscribe;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@MapperScan("com.iot.agriculture_iot.mapper")
@SpringBootApplication
public class AgricultureIotApplication {

    public static void main(String[] args) {

        ApplicationContext applicationContext = SpringApplication.run(AgricultureIotApplication.class, args);
        MqttSubscribe subscribe = new MqttSubscribe(applicationContext.getBean(IotMapper.class));
        subscribe.start();
    }

}
