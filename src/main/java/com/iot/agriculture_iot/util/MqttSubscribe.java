package com.iot.agriculture_iot.util;

import ch.qos.logback.core.util.FileUtil;
import com.iot.agriculture_iot.bean.IOT;
import com.iot.agriculture_iot.mapper.IotMapper;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HexFormat;
import java.util.List;
import java.util.UUID;
public class MqttSubscribe extends Thread {
    IotMapper iotMapper;

    public MqttSubscribe(IotMapper iotMapper) {
        this.iotMapper = iotMapper;
    }

    @Override
    public void run() {
        start();
    }

    public void start() {
        String broker = "tcp://106.14.149.61:1883"; // MQTT broker 地址
        String clientId = UUID.randomUUID().toString(); // 客户端ID
        String topic = "env/device1"; // 订阅的主题
        List<IOT> iotBatch = new ArrayList<>();

        try {
            MqttClient client = new MqttClient(broker, clientId, new MemoryPersistence());

            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);

            System.out.println("Connecting to broker: " + broker);
            client.connect(connOpts);
            System.out.println("Connected");

            System.out.println("Subscribing to topic: " + topic);
            client.subscribe(topic);
            System.out.println("Subscribed");

            client.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {
                    System.out.println("Connection lost!");
                    while (true) {
                        try {
                            client.connect(connOpts);
                            client.subscribe(topic);
                            System.out.println("MQTT重连成功");

                            break;
                        } catch (MqttException e) {
                            e.printStackTrace();
                            try {
                                System.out.println("MQTT 正在重连");
                                Thread.sleep(1000);
                            } catch (InterruptedException ex) {
                                throw new RuntimeException(ex);
                            }
                        }
                    }
                }

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    // 使用BigInteger将byte[]数组转换为16进制字符串
                    BigInteger bigInt = new BigInteger(1, message.getPayload());
                    String hexString = bigInt.toString(16);
                    //解析数据，lbs格式，每两位一段信息
                    List<String> temp = new ArrayList<>();
                    for (int i = 0; i < hexString.length(); i += 2) {
//                        System.out.println(hexString.substring(i, i + 2));
                        temp.add(hexString.substring(i, i + 2));
                    }
                    // 5个U8数据 （温度，湿度，烟雾，光照，土壤湿度）
                    if (temp.get(2).equals("10")) {
                        IOT iot = IOT.formatBy(temp);

                        iotMapper.insert(iot);
//                        iotBatch.add(iot);
//                        System.out.println(iot);
                    }
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    // Not used in this example
                }
            });


        } catch (MqttException me) {
            System.out.println("Reason code: " + me.getReasonCode());
            System.out.println("Message: " + me.getMessage());
            System.out.println("Localized message: " + me.getLocalizedMessage());
            System.out.println("Cause: " + me.getCause());
            System.out.println("Exception: " + me);
            me.printStackTrace();
        }
    }
}

