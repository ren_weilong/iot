package com.iot.agriculture_iot.util;

public class ApiResponse {
    private int code;
    private String message;
    private Object data;

    public ApiResponse() {

    }

    public static ApiResponse success() {
        ApiResponse ApiResponse = new ApiResponse();
        ApiResponse.setCode(200);
        ApiResponse.setMessage("success");

        return ApiResponse;
    }

    public static ApiResponse success(Object data) {
        ApiResponse ApiResponse = new ApiResponse();
        ApiResponse.setCode(200);
        ApiResponse.setMessage("success");
        ApiResponse.setData(data);

        return ApiResponse;
    }

    public static ApiResponse fail() {
        ApiResponse ApiResponse = new ApiResponse();
        ApiResponse.setCode(500);
        ApiResponse.setMessage("fail");

        return ApiResponse;
    }

    public static ApiResponse fail(String message) {
        ApiResponse ApiResponse = new ApiResponse();
        ApiResponse.setCode(500);
        ApiResponse.setMessage(message);

        return ApiResponse;
    }


    public void setData(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

